import React from "react";

const Footer = () => {
  return (
    <footer
      style={{
        backgroundColor: "#f2f2f2",
        padding: "20px",
        textAlign: "center",
      }}
    >
      <div style={{ display: "flex", justifyContent: "center" }}>
        <a href="#">Home</a>
        <span style={{ margin: "0 10px" }}>|</span>
        <a href="#">About</a>
        <span style={{ margin: "0 10px" }}>|</span>
        <a href="#">Services</a>
        <span style={{ margin: "0 10px" }}>|</span>
        <a href="#">Contact</a>
      </div>
    </footer>
  );
};

export default Footer;
