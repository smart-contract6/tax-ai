import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Carousel } from "../../../components";
import Strpper from "../../../components/Staper";

const CompanyLogin = () => {
  const [projectName, setProjectName] = useState(null);

  const navigate = useNavigate();

  const handleLogin = () => {
    if (projectName) {
      localStorage.setItem("project", projectName);
      navigate("/upload-document");
    } else {
      alert("please enter project name");
    }
  };

  return (
    <div className="container-fluid">
      <div className="row" style={{ padding: 20 }}>
        <Strpper activeStep={0} />
        <div className="col-6 d-flex align-items-center">
          <div className="ss-inr-hero position-relative">
            <div className="page-back-btn">
              <button onClick={() => window.location.replace("/")}>
                <i className="bi bi-chevron-left"></i>
              </button>
            </div>
            <h2>Welcome to SmartSync!!</h2>
            <h4>Create a new project</h4>

            <div className="frm-hldr">
              <div className="frm-fild d-flex flex-column">
                <label for="">Please Enter Project Name</label>
                <input
                  type="text"
                  placeholder="March-2024"
                  name=""
                  id=""
                  onChange={(e) => setProjectName(e.target.value)}
                />
              </div>
              <button className="cmn-btn" onClick={handleLogin}>
                Save & Next
              </button>
              <p className="text-center">
                <a href="#!">Don't have a account?</a>
              </p>
            </div>
          </div>
        </div>

        <div className="col-6">
          <div
            id="carouselExampleCaptions"
            className="carousel slide carousel-dark ss-carosel"
            data-bs-ride="carousel"
          >
            <div className="ss-navbtn-hldr">
              <button
                className="carousel-control-prev"
                type="button"
                data-bs-target="#carouselExampleCaptions"
                data-bs-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Previous</span>
              </button>
              <button
                className="carousel-control-next"
                type="button"
                data-bs-target="#carouselExampleCaptions"
                data-bs-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Next</span>
              </button>
            </div>

            <Carousel />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompanyLogin;
