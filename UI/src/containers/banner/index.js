import React from "react";
import { BANNED_HEADER, BANNED_MESSAGE } from "../../utils/constants";

const Banner = () => {
  return (
    <div className="mainDiv">
      <section className="hm-hero-desc">
        <h1>{BANNED_HEADER}</h1>
        <p>{BANNED_MESSAGE}</p>
        <div className="hero-btnhldr">
          <button
            className="cmn-btn"
            onClick={() => window.location.replace("/#/scenario-name")}
          >
            Try Now <i className="bi bi-chevron-right"></i>
          </button>
          <button className="cmn-btn">Watch a demo</button>
        </div>
      </section>
    </div>
  );
};

export default Banner;
